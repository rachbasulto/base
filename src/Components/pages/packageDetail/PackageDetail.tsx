import React from "react"
import Banner from "../../global/banner/Banner"
import DetailCard from "./detailsCard/DetailCard"


function PackageDetail() {
  const images: [] = []

  return (
    <div>
      <div>
        <Banner images={images} />
      </div>
      <div>
        <DetailCard />
      </div>
    </div>
  )
}

export default PackageDetail