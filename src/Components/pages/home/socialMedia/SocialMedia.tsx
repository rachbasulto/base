import { Row, Col } from 'reactstrap';

import * as Styled from './SocialMedia.style'

import { Props } from "./SocialMedia.interface";
import React from 'react';

function SocialMedia({ socialMedia }: Props) {
    const length = 12 / socialMedia.length
    return (
        <Styled.Container>
            <Row>
                {socialMedia.map((item) => {
                    <Col sm={length}>
                        {item.icon}
                    </Col>
                    {
                        item.correo && (
                            icono { item.correo }
                    )}
                })}
            </Row>
        </Styled.Container>
    );
}

export default SocialMedia