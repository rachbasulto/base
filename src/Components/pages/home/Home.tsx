import React from "react"
import HomeBanner from "./banner/Banner"
import PackagesCards from "./cards/Cards"
import HomeBar from "./homeBar/HomeBar"
import SocialMedia from "./socialMedia/SocialMedia"


function Home() {
    const images: [] = []
    const socialMedia: [] = []
    const packagesInfo: [] = [] //se trae de la base

    return (
        <div>
            <div>
                <HomeBanner images={images} />
                <SocialMedia socialMedia={socialMedia} />
            </div>
            <div>
                <HomeBar />
            </div>
            <div>
                <PackagesCards packagesInfo={packagesInfo} />
            </div>
        </div>
    )
}

export default Home