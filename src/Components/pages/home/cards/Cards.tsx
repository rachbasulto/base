
import React from "react";
import Card from "./card/Card";

import * as Styled from './Cards.style'

import { Props } from "./Cards.interface";

function PackagesCards({ packagesInfo }: Props) {
    return (
        <Styled.Container>
            {packagesInfo.map((packageItem) => {
                <Card cardInfo={packageItem.cardInfo}>

                </Card>
            })}
        </Styled.Container>
    );
}

export default PackagesCards